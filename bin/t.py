#!/usr/bin/env python3

import argparse
import lxml.etree as et


def remove_element(element):
    parent = element.getparent()
    previous = element.getprevious()
    
    if previous is not None:
        previous.tail = (previous.tail or '') + (element.tail or '')
    else:
        parent.text = (parent.text or '') + (element.tail or '')
    
    parent.remove(element)


argument_parser = argparse.ArgumentParser(description='Extract plain text from retro-dict TEI XML.')
argument_parser.add_argument('file', metavar='FILE', type=str, help='TEI XML input file')
argument_parser.add_argument('-k', '--keep', metavar='CHOICE', choices=('sic', 'corr', 'both'), default='corr', help='sic: dicard corrections and retain the original text, corr: apply recorded corrections to the original text (this is the default), both: retain the original text *and* the corrections')
argument_parser.add_argument('-t', '--tokenize', action='store_true', default=False, help='perform naive whitespace tokenization (one token per line, no blank lines)')
arguments = argument_parser.parse_args()


tree = et.parse(arguments.file)
root = tree.getroot()

if arguments.keep == 'corr':
    for sic in root.findall('.//{http://www.tei-c.org/ns/1.0}sic'):
        remove_element(sic)
elif arguments.keep == 'sic':
    for corr in root.findall('.//{http://www.tei-c.org/ns/1.0}corr'):
        remove_element(corr)
else:
    for sic in root.findall('.//{http://www.tei-c.org/ns/1.0}sic'):
        sic.tail = ' ' + (sic.tail or '')

text = ''.join(et.ETXPath('.//text()')(root))

if arguments.tokenize:
    print('\n'.join(text.split()))
else:
    print(text)
