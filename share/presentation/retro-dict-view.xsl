<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
  xmlns:tei="http://www.tei-c.org/ns/1.0">

  <xsl:output method="html" media-type="text/html" cdata-section-elements="script style" indent="no"
    encoding="utf-8"/>
  <xsl:strip-space elements="tei:choice"/>

  <xsl:template match="tei:TEI">
    <html>
      <xsl:apply-templates/>
    </html>
  </xsl:template>

  <xsl:template match="tei:teiHeader">
    <head>
      <title>
        <xsl:value-of select="//tei:titleStmt/tei:title"/>
      </title>
      <link rel="stylesheet" href="../../share/presentation/retro-dict.css"></link>
    </head>
  </xsl:template>

  <xsl:template match="tei:text">
    <body>
      <xsl:apply-templates/>
    </body>
  </xsl:template>

  <xsl:template match="tei:titlePage">
    <div class="title_page">
      <xsl:apply-templates/>
    </div>
  </xsl:template>

  <xsl:template match="tei:div/tei:head">
    <h1 class="{translate(./@rendition, '#', '')}">
      <xsl:apply-templates/>
    </h1>
  </xsl:template>

  <xsl:template match="tei:div/tei:div/tei:head">
    <h2 class="{translate(./@rendition, '#', '')}">
      <xsl:apply-templates/>
    </h2>
  </xsl:template>

  <xsl:template match="tei:div/tei:div/tei:div/tei:head">
    <h3 class="{translate(./@rendition, '#', '')}">
      <xsl:apply-templates/>
    </h3>
  </xsl:template>

  <xsl:template match="tei:div/tei:div/tei:div/tei:div/tei:head">
    <h4 class="{translate(./@rendition, '#', '')}">
      <xsl:apply-templates/>
    </h4>
  </xsl:template>

  <xsl:template match="tei:list/tei:head">
    <p class="{translate(./@rendition, '#', '')}">
      <xsl:apply-templates/>
    </p>
  </xsl:template>

  <xsl:template match="tei:p">
    <p class="{translate(./@rendition, '#', '')}">
      <xsl:apply-templates/>
    </p>
  </xsl:template>

  <xsl:template match="tei:item">
    <p class="{translate(./@rendition, '#', '')}">
      <xsl:apply-templates/>
    </p>
  </xsl:template>

  <xsl:template match="tei:space[@dim='horizontal']">
    <span class="{translate(./@rendition, '#', '')} hspace"/>
  </xsl:template>

  <xsl:template match="tei:space[@dim='vertical']">
    <div class="#vspace">&#160;</div>
  </xsl:template>

  <xsl:template match="tei:hi">
    <span class="{translate(./@rendition, '#', '')}">
      <xsl:apply-templates/>
    </span>
  </xsl:template>
  
  <xsl:template match="tei:p/tei:pb">
    <span class="page_number">p.&#160;<xsl:value-of select="./@n"/>&#160;–&#160;<xsl:value-of select="./@facs"/></span>
  </xsl:template>
  
  <xsl:template match="tei:pb">
    <div class="page_number">p.&#160;<xsl:value-of select="./@n"/>&#160;–&#160;<xsl:value-of select="./@facs"/></div>
  </xsl:template>
  
  <xsl:template match="tei:lb">
    <br />
  </xsl:template>
  
  <xsl:template match="tei:milestone">
    <hr />
  </xsl:template>
  
  <xsl:template match="tei:sic">
    <span class="editorial s"><xsl:apply-templates/>|</span>
  </xsl:template>
  <xsl:template match="tei:corr">
    <span class="editorial"><xsl:apply-templates/></span>
  </xsl:template>  

  <xsl:template match="tei:gap">
    <span class="gap">??</span>
  </xsl:template>

  <xsl:template match="tei:table">
	  <table><xsl:apply-templates/></table>
  </xsl:template>

  <xsl:template match="tei:row">
	  <tr><xsl:apply-templates/></tr>
  </xsl:template>
  
  <xsl:template match="tei:cell">
	  <td class="{translate(./@rendition, '#', '')}"><xsl:apply-templates/></td>
  </xsl:template>

  <xsl:template match="tei:list">
	  <ul><xsl:apply-templates/></ul>
  </xsl:template>
  <xsl:template match="tei:item">
	  <li><xsl:apply-templates/></li>
  </xsl:template>

  <!-- footnotes, margin notes -->
  <xsl:template match="tei:note">
	  <span class="note-mark"><xsl:value-of select="./@n"/>
	  	<span class="note-text">
			<xsl:value-of select="./@n"/>&#160;
		  	<xsl:apply-templates/>
		</span>
	  </span>
  </xsl:template>

</xsl:stylesheet>
