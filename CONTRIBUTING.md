# Contributing to retro-dict

Anyone interested in contributing to `retro-dict` is encouraged to contact
me via email (herold@bbaw.de).

Also, if you re-use parts of the data provided here I would be happy to learn about _your_ project.
