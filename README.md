# `retro-dict`: historical etymological dictionaries

`retro-dict` is a collection of historical etymological dictionaries.

## Transcription principles

Transcription and typographical markup follow the guidelines
of the DTA base format which was developed for the [German Text Archive](https://www.deutschestextarchiv.de) (DTA) on the basis of the Text Encoding Initiative's
[TEI P5 guidelines](https://www.tei-c.org).
You can find the upstream version of the DTA base format
(schema and documentation in ODD) at
https://github.com/deutschestextarchiv/dtabf. `retro-dict` keeps
an unmodified set of schemas
(Relax NG and Schematron, under `share/validation`)
for convenience and easier off-line use of the collection's data.
Upstream changes to the DTA base format schemas will lead to
updates of existing `retro-dict` data.

During transcription, all font properties are preserved.
Printed characters are generally not normalized
to keep (the partly) idiosyncratic transliterations as close to the
printed original as possible. However, features that are only accountable to
media-specific constraints are removed: line breaks are not preserved
except in verse quotations.
Hyphenation is also not preserved, i.e. all hypenated words are
faithfully reconstructed. Words that are split across pages are only recorded
on the page they begin on.
Signature marks, running titles or the exact
location of page numbers are not preserved. Page and column beginnings
are still recorded, though.

## Viewing the data

In order to view the transcripts in a web browser after cloning
or checking out this repository you may have to set up a webserver
running locally on your machine in order to have the XSL applied automatically.
An easy way to do this is:

```user@host:retrodict$ python3 -m http.server```

and then pointing your browser to http://localhost:8000/.
Depending on your browser, you may also be able to directly open the files
from disk and have them rendered via XSLT.

## Availability of scanned images

In general, scanned images are not provided by the `retro-dict` project. However, all scans from
https://www.archive.org/ that were used for the transcription are referenced in the metadata sections of the
transcripts. In cases where individual scans from https://www.archive.org/ are missing or partly broken,
substitute scans _may_ be provided in `tei-src/*/scan` directories.
